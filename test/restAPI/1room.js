const request = require('supertest');
const app = require('../../app.js');
const should = require('chai').should();

let savedroomid;
describe('GET /room/', () => {
    it('should return 200 OK', (done) => {
        request(app)
        .get('/api/room/')
        .expect(200, done);
    }).timeout(2000);
    
    
    describe('GET /api/rooms/',()=>{
        it('should return 404', (done) => {
            request(app)
            .get('/api/rooms/')
            .expect(404, done);
        }).timeout(2000);
    });
});

describe('PUT /api/room',()=>{
    it('should return 400 for empty data',(done)=>{
        request(app)
        .put('/api/room')
        .expect(400,done);
    });
    
    it('should return the saved room js',(done)=>{
        request(app)
        .put('/api/room')
        .send({roomtype:"Rental",roomname:"testroom"})
        .expect(200)
        .then(response => {
            try{
                response.text.should.be.a('string');
                response.text.should.have.string('room');
                 savedroomid=response.text.split("/")[2];
                 module.exports=savedroomid;
                done();
            }catch(err){
                done(err);
            }
        });
    });
})

