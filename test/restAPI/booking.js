const request=require("supertest");
const User = require('../../models/User.js');
const app= require('../../app.js');

const should = require('chai').should();

//added for getting roomid

let cookie;
describe('POST /api/booking',()=>{
    before(function(done){
        let user = new User({
            name: 'Fake User',
            email: 'test@test.com',
            password: 'password'
        });
        
        user.save(function(err) {
            if (err) return done(err);
            request(app)
            .post('/login')
            .send({ email: "test@test.com", password:'password' })
            .expect(302)
            .end(function(err,res){
                cookie = res.headers['set-cookie'];
                done();        
            });     
        });
    });
    
    it('should return 400',(done)=>{
        request(app)
        .post('/api/booking/')
        .send({})
        .set('cookie', cookie)
    .set('content-type', 'application/json')
        .expect(400)
        .then(response => {
            try{
                response.should.be.an('object');
                done();
            }catch(err){
                done(err);
            }
        });
    }).timeout(3000);

it('should return 200 while giving proper object',(done)=>{
const roomtestid=require('./1room.js');

 let   postdata = {starttime:Date.now(),endtime:Date.now(),
        room:roomtestid}
 request(app)
         .post('/api/booking/')
         .set('cookie', cookie)
         .send(postdata)
        .expect(200)
        .then(response=>{
             response.text.should.have.string('booking');
             done();
        })

    });
})