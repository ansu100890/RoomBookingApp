const request = require('supertest');
const app = require('../app.js');
const mongoose = require('mongoose');




describe('GET /', () => {
  before( function (done) {
    this.timeout(5000);
    mongoose.connection.dropDatabase(function(){
      console.log("TEST:db dropped");
      done();
    })   
  })
  
  it('should return 200 OK', (done) => {
    request(app)
    .get('/')
    .expect(200, done);
  }).timeout(5000);
});

describe('GET /login', () => {
  it('should return 200 OK', (done) => {
    request(app)
    .get('/login')
    .expect(200, done);
  });
});

describe('GET /signup', () => {
  it('should return 200 OK', (done) => {
    request(app)
    .get('/signup')
    .expect(200, done);
  });
});


describe('GET /contact', () => {
  it('should return 200 OK', (done) => {
    request(app)
    .get('/contact')
    .expect(200, done);
  });
});

describe('GET /random-url', () => {
  it('should return 404', (done) => {
    request(app)
    .get('/reset')
    .expect(404, done);
  });
});
