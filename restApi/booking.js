const Booking= require("../models/Booking.js");
const _ =require('lodash');
const bookingrouter =require("express").Router();


bookingrouter.post('/:id',(req,res)=>{    
    if(!req.params.id){
        //throw error         
    }else{
        //update 
    }
    
    res.status(200);
});

bookingrouter.post('/',(req,res)=>{    
    let bookingData=_.cloneDeep(req.body);
    bookingData.madeBy=req.user.id;
    let bookingobj= new Booking(bookingData);
    
    bookingobj.save(function(err,savedBooking){
        if(err && err.name=='ValidationError'){
            res.status(400).send(err.errors);
        }else if(err){
            res.status(500).send("error at server end");
        }
        else{
            res.status(200).send("/booking/"+savedBooking.id);
        }
    });
})

module.exports=bookingrouter;