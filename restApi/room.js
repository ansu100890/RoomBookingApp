const roomroute=require("express").Router();
const roomModel=require('../models/room.js')

roomroute.get('/:id',function(req,res){
    
});

roomroute.get('/',function(req,res){
    console.log("getting rooms");
    roomModel.find( (err, rooms) => {
        if (err) return console.error(err);
        console.log(rooms);
        res.json(rooms);
    })
});

roomroute.post('/:id',function(req,res){
    
});

roomroute.put('/',function(req,res){
    let newRoom= new roomModel(req.body);
    roomModel.create(newRoom).then((savedRoom)=>{
        console.log(savedRoom);
        res.status(200).send("/room/"+savedRoom.id);
    }).catch((err)=>{
        if(err.name=="MongoError"){
            res.status(400).send(err);
        }
        else{
            res.status(400).send(err.message);
        }
    })
});

roomroute.delete('/id',function(req,res){
    
});

module.exports = roomroute;

