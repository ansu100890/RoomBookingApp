const mongoose=require("mongoose");

const roomschema= new mongoose.Schema({
roomtype:String,
roomname:{
        type: String,
        required: true,
        unique:true
      }
});

const Room =mongoose.model('Room',roomschema);

module.exports=Room;

