const mongoose = require('mongoose');

const bookingschema = new mongoose.Schema({

    starttime:{
        type: Date,
        required: true,        
      },
    endtime:{
        type: Date,
        required: true,        
      },
    event:{type:mongoose.Schema.Types.ObjectId,ref:'Event'},
    room:{type:mongoose.Schema.Types.ObjectId,ref:'Room',required:true},
    madeBy:{type:mongoose.Schema.Types.ObjectId,ref:'User',required:true}    
});

const Booking=mongoose.model('Booking',bookingschema);


module.exports= Booking;