const mongoose=require("mongoose");

const eventschema=new mongoose.Schema({
    eventname:string,
    attendees:[{
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'}]
    });

    const Event= mongoose.model('Event',eventschema);

    module.exports=Event;